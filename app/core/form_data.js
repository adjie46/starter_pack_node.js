const util = require("util");
const multer = require("multer");
const maxSize = 1 * 1024 * 1024;

const { v4: uuidv4 } = require("uuid");

const config = require("../config/config");
const orderid = require("order-id")(config.jwtSecret);

const path = require("path");
const { v5: uuidv5 } = require("uuid");
const crypto = require("crypto");

const whitelist = ["image/png", "image/jpeg", "image/jpg", "application/pdf"];

async function randomString(length) {
	return new Promise(async (resolve, reject) => {
		try {
			var result = "";
			var characters =
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var charactersLength = characters.length;
			for (var i = 0; i < length; i++) {
				result += characters.charAt(
					Math.floor(Math.random() * charactersLength)
				);
			}
			resolve(result);
		} catch (error) {
			reject(error);
		}
	});
}

let storage = multer.diskStorage({
	destination: function (req, file, cb) {
		if (file) {
			cb(null, "./public/upload/temp");
		} else {
			console.log("TIDAK ADA");
		}
	},
	filename: async function (req, file, cb) {
		uuidGen = uuidv5(await randomString(10), config.nameSpace);
		const md5Hash = crypto.createHash("md5").update(uuidGen).digest("hex");

		cb(null, `FILE_DRAFT_${md5Hash}` + path.extname(file.originalname));
	},
});

var uploadFile = multer({
	storage: storage,
	fileFilter: (req, file, cb) => {
		if (!whitelist.includes(file.mimetype)) {
			return cb(new Error("File tidak diizinkan"));
		}
		cb(null, true);
	},
	limits: {
		fileSize: maxSize,
	},
}).any();

let uploadFileMiddleware = util.promisify(uploadFile);
module.exports = uploadFileMiddleware;
