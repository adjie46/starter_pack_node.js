"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.addColumn(
				"profiles", // table name
				"latitude", // new field name
				{
					type: Sequelize.STRING,
					allowNull: true,
					after: "firebase_token",
				}
			),
			queryInterface.addColumn(
				"profiles", // table name
				"longitude", // new field name
				{
					type: Sequelize.STRING,
					allowNull: true,
					after: "firebase_token",
				}
			),
		]);
	},

	async down(queryInterface, Sequelize) {
		return Promise.all([
			queryInterface.removeColumn("profiles", "latitude"),
			queryInterface.removeColumn("profiles", "longitude"),
		]);
	},
};
