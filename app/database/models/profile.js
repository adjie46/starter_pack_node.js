"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class profile extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			profile.belongsTo(models.auth, {
				foreignKey: "id_auth",
				as: "auth",
				onDelete: "CASCADE",
				hooks: true,
			});

			profile.hasMany(models.auth_device, {
				foreignKey: "id_auth",
				targetKey: "id_auth",
				sourceKey: "id_auth",
				as: "device",
				onDelete: "CASCADE",
				hooks: true,
			});
			profile.hasMany(models.permission, {
				foreignKey: "id_auth",
				targetKey: "id_auth",
				sourceKey: "id_auth",
				as: "permission",
				onDelete: "CASCADE",
				hooks: true,
			});
			profile.belongsTo(models.setting, {
				foreignKey: "id_auth",
				targetKey: "id_auth",
				as: "setting",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	profile.init(
		{
			id_auth: DataTypes.STRING,
			full_name: DataTypes.STRING,
			NIK: DataTypes.STRING,
			email: DataTypes.STRING,
			phone: DataTypes.STRING,
			photo_profile: DataTypes.STRING,

			firebase_token: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "profile",
		}
	);
	return profile;
};
