"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class permission extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			permission.belongsTo(models.profile, {
				foreignKey: "id_auth",
				targetKey: "id_auth",
				sourceKey: "id_auth",
				as: "permission",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	permission.init(
		{
			id_auth: DataTypes.STRING,
			permission_name: DataTypes.STRING,
			permission_status: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "permission",
		}
	);
	return permission;
};
