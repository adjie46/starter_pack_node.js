const library = require("../core/library");

exports.dashboard_pages = async (req, res) => {
	let pages = req.query;
	let dataDashboard = {};
	let token = req.session.tokenLogin;
	let payload = req.decrypted.payload;

	myProfile = await library.model.auth.findOne({
		attributes: ["id", "user_type", "status"],
		include: [
			{
				model: library.model.profile,
				as: "profile",
			},
		],
		where: {
			id: await library.decrypt(payload.user_login_auth),
		},
	});

	if (myProfile.user_type == "-1") {
		user_type_category = "Operator Utama";
	} else {
		user_type_category = "";
	}

	if (
		library.fs.existsSync(
			`public/upload/photoProfile/${
				myProfile.profile.photo_profile.split("/")[4]
			}`
		)
	) {
		photoProfile = myProfile.profile.photo_profile;
	} else {
		photoProfile = "/dist/images/profile/user-1.jpg";
	}

	if (library.isEmpty(myProfile.profile.opd)) {
		opdName = "-";
	} else {
		opdName = myProfile.profile.opd.opd_name;
	}
	dataDashboard.users = {
		user_name: myProfile.profile.full_name,
		user_type: myProfile.user_type,
		user_type_category: user_type_category,
		user_email: myProfile.profile.email,
		user_phone: myProfile.profile.phone,
		user_photo: photoProfile,
		opd_id: myProfile.profile.opd_id,
		opd_name: opdName,
		id: await library.encrypt(myProfile.id),
	};

	if (library.isEmpty(pages)) {
		dataDashboard.current_pages = "dashboard";
	} else {
		dataDashboard.current_pages = pages.pages;
	}

	var fileContents;
	var showPages, showCss, showJs;

	try {
		fileContents = library.fs
			.readdirSync(
				`./app/view/pages/admin/content/${dataDashboard.current_pages}`
			)
			.forEach((file) => {
				showPages = `admin/content/${dataDashboard.current_pages}/index`;
			});

		fileJs = library.fs
			.readdirSync(
				`./app/view/pages/admin/content/${dataDashboard.current_pages}`
			)
			.forEach((file) => {
				showJs = `admin/content/${dataDashboard.current_pages}/js`;
			});

		fileCss = library.fs
			.readdirSync(
				`./app/view/pages/admin/content/${dataDashboard.current_pages}`
			)
			.forEach((file) => {
				showCss = `admin/content/${dataDashboard.current_pages}/css`;
			});
	} catch (err) {
		showPages = "admin/no_item";
		showCss = `admin/no_item`;
		showJs = `admin/no_item`;
	}

	if (dataDashboard.current_pages == "detail_opd") {
		dataDashboard.current_id = pages.id;

		detailOpd = await library.model.opd.findOne({
			where: {
				id: pages.id,
			},
		});

		dataDashboard.opd = detailOpd;
	} else if (dataDashboard.current_pages == "profile") {
		dataDashboard.current_id = pages.id;
	} else if (dataDashboard.current_pages == "pegawai") {
		dataDashboard.current_id = pages.id;
		opd = await library.model.opd.findAll();
		dataDashboard.dataOpd = opd;
	} else if (dataDashboard.current_pages == "tracking") {
		dataDashboard.current_id = pages.id;
		pegawai = await library.model.auth.findAll({
			include: [
				{
					model: library.model.profile,
					as: "profile",
				},
			],
			where: {
				user_type: 3,
			},
		});
		dataDashboard.dataPegawai = pegawai;
	} else if (dataDashboard.current_pages == "upload_document") {
		dataDashboard.current_id = pages.id;

		ppt = await library.model.ppt.findAll({
			include: [
				{
					model: library.model.profile,
					as: "profile",
				},
			],
			order: [[library.model.profile, "full_name", "asc"]],
		});

		var objectdlu = JSON.stringify(ppt);
		var ppt = JSON.parse(objectdlu);

		await library.asyncForEach(ppt, async (element, index) => {
			await library.waitFor(1);

			photo_profile = "";

			if (
				library.fs.existsSync(
					`public/upload/photoProfile/${
						element.profile.photo_profile.split("/")[4]
					}`
				)
			) {
				photo_profile = element.profile.photo_profile;
			} else {
				photo_profile = "../../dist/images/profile/user-1.jpg";
			}

			ppt[index].profile.photo_profile = photo_profile;
		});

		dataDashboard.dataPpt = ppt;
	} else if (dataDashboard.current_pages == "user_profile") {
		settings = await library.model.setting.findOne({
			where: {
				id_auth: myProfile.id,
			},
		});

		dataDashboard.settings = settings;
	} else if (dataDashboard.current_pages == "dashboard") {
	}

	return res.render("pages/admin/dashboard", {
		csrfToken: req.csrfToken(),
		web_title: "Dashboard - " + req.web_setting.web_name,
		web_desc: req.web_setting.web_desc,
		web_author: req.web_setting.web_author,
		web_keywords: req.web_setting.web_keywords,
		web_url: `${req.web_setting.web_url}/dashboard`,
		web_icon: req.web_setting.web_icon,
		datas: dataDashboard,
		content: function () {
			if (!library.isEmpty(myProfile)) {
				return showPages;
			} else {
				return "admin/no_item";
			}
		},
		includeCss: function () {
			if (!library.isEmpty(myProfile)) {
				return showCss;
			} else {
				return "admin/no_item";
			}
		},
		includeJs: function () {
			if (!library.isEmpty(myProfile)) {
				return showJs;
			} else {
				return "admin/no_item";
			}
		},
	});
};
