const library = require("../core/library");
const { Op } = require("sequelize");

exports.profileAction = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		const { id, action } = req.params;

		if (action == "profile") {
			profile = await library.model.auth.findByPk(id, {
				attributes: ["id", "user_auth", "user_type"],
				include: [
					{
						attributes: [
							"full_name",
							"email",
							"phone",
							"photo_profile",
							"opd_id",
							"id_auth",
							"nik",
						],
						model: library.model.profile,
						as: "profile",
						include: [
							{
								attributes: ["opd_name"],
								model: library.model.opd,
								as: "opd",
							},
							{ model: library.model.auth_device, as: "device" },
							{ model: library.model.permission, as: "permission" },
							{ model: library.model.setting, as: "setting" },
							{ model: library.model.ppt, as: "ppt" },
						],
					},
				],
				paranoid: false,
			});

			if (!library.isEmpty(profile)) {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "Data Ditemukan",
					data: profile,
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Data Tidak Ditemukan",
					data: {},
				});
			}
		} else if (action == "delete_device") {
			deleteDevice = await library.model.auth_device.destroy({
				where: {
					id: id,
				},
			});

			if (deleteDevice) {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "Device Berhasil dihapus",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Device Gagal di Hapus",
				});
			}
		} else if (action == "bsre_profile") {
			bsre_profile = await library.model.profile.findOne({
				include: [
					{ model: library.model.ppt, as: "ppt" },
					{
						attributes: ["permission_name"],
						model: library.model.permission,
						as: "permission",
					},
				],
				where: {
					id_auth: id,
				},
			});

			if (library.isEmpty(bsre_profile.ppt)) {
				bsreResponse = {
					tte_status: "Tidak Tersedia",
					visual_tte: "Tidak Tersedia",
					tte_expired: "Tidak Tersedia",
					tte_type: "Tidak Tersedia",
					user_permission: [],
				};

				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "Data ditemukan",
					data: bsreResponse,
				});
			} else {
				bsreProfile = await library.apiBsre(
					bsre_profile.ppt.nik,
					"user/profile/"
				);

				jenis_sertifikat = "Berlum Terdaftar";
				berlaku_sampai = "Berlum Terdaftar";
				status_user = "Berlum Terdaftar";

				if (!library.isEmpty(bsreProfile.data)) {
					if (bsreProfile.data.sertifikat.length > 0) {
						berlaku_sampai = bsreProfile.data.sertifikat[0].berlaku_sampai;
						jenis_sertifikat = bsreProfile.data.sertifikat[0].jenis_sertifikat;
					} else {
						jenis_sertifikat = "Berlum Terdaftar";
						berlaku_sampai = "Berlum Terdaftar";
						status_user = "Berlum Terdaftar";
					}

					status_user = bsreProfile.data.status_user;
				}

				bsreResponse = {
					tte_status: status_user,
					visual_tte: bsre_profile.ppt.visual_tte,
					tte_expired: berlaku_sampai,
					tte_type: jenis_sertifikat,
					user_permission: bsre_profile.permission,
				};

				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "Data ditemukan",
					data: bsreResponse,
				});
			}
		} else if (action == "change_password") {
			_id = await library.decrypt(datas._id);

			if (
				!library.isEmpty(datas.old_password) ||
				library.isEmpty(datas.new_password) ||
				library.isEmpty(datas.konfirm_password)
			) {
				if (datas.new_password == datas.konfirm_password) {
					user = await library.model.auth.findOne({
						where: {
							id: _id,
						},
					});

					oldPassword = await library.decrypt(user.user_pass);

					if (!library.isEmpty(user)) {
						if (datas.old_password == oldPassword) {
							newPassword = {
								user_pass: await library.encrypt(datas.new_password),
							};

							library.model.auth.update(newPassword, {
								where: {
									id: _id,
								},
							});

							req.session.destroy();

							return res.status(library.responseStatus.OK).send({
								success: true,
								code: library.responseStatus.OK,
								message:
									"Password Anda Berhasil di rubah, silahkan login ulang",
							});
						} else {
							return res.status(library.responseStatus.OK).send({
								success: false,
								code: library.responseStatus.OK,
								message: "Password Saat ini Salah!, silahkan coba lagi",
							});
						}
					} else {
						return res.status(library.responseStatus.OK).send({
							success: false,
							code: library.responseStatus.OK,
							message: "Pengguna tidak ditemukan",
						});
					}
				} else {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message:
							"Password Baru dan Konfirmasi Password Anda Tidak Sama, Silahkan coba lagi!",
					});
				}
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Form Belum Lengkap",
				});
			}
		} else if (action == "notif_android_true") {
			_id = await library.decrypt(id);

			notification = {
				notification: 1,
			};

			library.model.setting.update(notification, {
				where: {
					id_auth: _id,
				},
			});

			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "Notifikasi Android Berhasil diaktifkan",
			});
		} else if (action == "notif_android_false") {
			_id = await library.decrypt(id);

			notification = {
				notification: 0,
			};

			library.model.setting.update(notification, {
				where: {
					id_auth: _id,
				},
			});

			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "Notifikasi Android Berhasil dinonaktifkan",
			});
		} else if (action == "notif_wa_true") {
			_id = await library.decrypt(id);

			notification = {
				notif_wa: 1,
			};

			library.model.setting.update(notification, {
				where: {
					id_auth: _id,
				},
			});

			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "Notifikasi Android Berhasil diaktifkan",
			});
		} else if (action == "notif_wa_false") {
			_id = await library.decrypt(id);

			notification = {
				notif_wa: 0,
			};

			library.model.setting.update(notification, {
				where: {
					id_auth: _id,
				},
			});

			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "Notifikasi Android Berhasil dinonaktifkan",
			});
		} else {
			return res.status(library.responseStatus.badRequest).send({
				success: false,
				code: library.responseStatus.badRequest,
				message: "Link Salah",
			});
		}
	} catch (error) {
		console.log(error);
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.change_photo = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		params = JSON.parse(JSON.stringify(req.params));
		query = JSON.parse(JSON.stringify(req.query));
		file = JSON.parse(JSON.stringify(req.files));

		targetFileTTE = `public/upload/photoProfile/${file[0].filename}`;
		library.fs.rename(file[0].path, targetFileTTE, (error) => {
			if (error) {
				throw error;
			}
			if (library.fs.existsSync(file[0].path)) {
				library.fs.unlinkSync(file[0].path);
			}
		});

		newPhotoProfile = {
			photo_profile: `/assets/upload/photoProfile/${file[0].filename}`,
		};

		await library.model.profile.update(newPhotoProfile, {
			where: {
				id_auth: datas.id_auth,
			},
		});

		return res.status(library.responseStatus.OK).send({
			success: true,
			code: library.responseStatus.OK,
			message: "Photo Profile Berhasil Diganti",
		});
	} catch (error) {
		console.log(error);
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};
