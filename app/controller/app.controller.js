const e = require("express");
const library = require("../core/library");

exports.socket = async (req, res) => {
	console.log(req.io);
	req.io.on("connection", function (socket) {
		//log.info('socket.io connection made');
		console.log("socket.io connection made");
	});
};

exports.update_app = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));

		current_version = library.config.versiApps;

		if (current_version == datas.version_app) {
			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				update: false,
				message: "Aplikasi Sudah Paling Update",
			});
		} else {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				update: true,
				message: "Silahkan Lakukan Update Aplikasi!",
			});
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.login_android = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));

		users = await library.model.auth.findOne({
			include: [
				{
					model: library.model.profile,
					as: "profile",
					include: [
						{
							model: library.model.auth_device,
							as: "device",
						},
					],
				},
			],
			where: {
				[library.Op.or]: [
					{
						user_auth: datas.username_login,
					},
				],
			},
			paranoid: false,
		});

		if (library.isEmpty(users)) {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message: "Uupss, Pengguna Tidak Ditemukan",
			});
		} else {
			if (users.status == 0) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Pengguna Telah Dinonaktifkan",
				});
			} else if (users.status == 1) {
				if (users.user_type == "-1") {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: "User ini tidak dizinkan menggunakan aplikasi android",
					});
				} else if (users.user_type == "1") {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message:
							"Untuk Admin OPD Sabar ya, lagi dipersiapkan khusus untuk admin OPD",
					});
				} else {
					if (
						users.profile.device.length > 0 &&
						users.profile.device[0].imei_device != datas.imei
					) {
						return res.status(library.responseStatus.OK).send({
							success: false,
							code: library.responseStatus.OK,
							message: `Akun Anda Sudah Login di HP Lain (${users.profile.device[0].manufacturer} ${users.profile.device[0].type}), Silahkan Hubungi Admin Kominfo`,
						});
					} else {
						passDecrypt = await library
							.decrypt(users.user_pass)
							.catch((err) => {
								return res.status(library.responseStatus.serverError).send({
									success: false,
									code: library.responseStatus.serverError,
									message: "Error 101, Hubungi Admin",
								});
							});

						if (passDecrypt === datas.password_login) {
							var payload;
							if (users.user_type == "-1" || users.user_type == -1) {
								payload = {
									user_login_name: users.profile.full_name,
									user_login_auth: await library.encrypt(users.id),
									user_login_opd: "",
								};
							} else {
								payload = {
									user_login_name: users.profile.full_name,
									user_login_auth: await library.encrypt(users.id),
									user_login_opd: await library.encrypt(users.profile.opd_id),
								};
							}

							const secret = Buffer.from(library.config.hex, "hex");
							const encryptedJwt = await library.encryptedJwt(
								"login_action",
								payload,
								secret,
								library.config.jwtExpireMobile
							);

							newLogs = {
								id_auth: await library.encrypt(users.id),
								log_type: "Success",
								log_action: `${users.profile.full_name} berhasil login dari android`,
								log_os: req.headers["sec-ch-ua-platform"],
								log_devices: req.device.type,
								log_ip: req.ipInfo.ip,
							};

							library.logs(newLogs);

							newDevice = {
								id: await library.uuid(),
								id_auth: users.id,
								imei_device: datas.imei,
								hardware: datas.device_hardware,
								manufacturer: datas.device_manufacturer,
								type: datas.device_type,
								os_name: datas.device_os_name,
								status: 1,
							};

							const [device, created] =
								await library.model.auth_device.findOrCreate({
									where: {
										id_auth: users.id,
									},
									defaults: newDevice,
								});

							return res.status(library.responseStatus.OK).send({
								success: true,
								code: library.responseStatus.OK,
								message: "Yeay, Anda Berhasil Masuk",
								token: "Bearer " + encryptedJwt,
							});
						} else {
							return res.status(library.responseStatus.OK).send({
								success: false,
								code: library.responseStatus.OK,
								message: "Huft, Password Anda Salah, Silahkan Coba Lagi",
							});
						}
					}
				}
			}
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.getProfileById = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		id = await library.decrypt(req.decrypted.user_login_auth);

		users = await library.model.auth.findOne({
			attributes: ["id", "user_auth", "user_type", "status"],
			include: [
				{
					model: library.model.profile,
					as: "profile",
					attributes: {
						exclude: ["createdAt", "updatedAt"],
					},
					include: [
						{
							model: library.model.auth_device,
							as: "device",
							attributes: {
								exclude: ["createdAt", "updatedAt"],
							},
						},
						{
							model: library.model.opd,
							as: "opd",
							attributes: {
								exclude: ["createdAt", "updatedAt"],
							},
						},
						{
							model: library.model.permission,
							as: "permission",
							attributes: {
								exclude: ["createdAt", "updatedAt"],
							},
						},
						{
							model: library.model.ppt,
							as: "ppt",
							attributes: {
								exclude: ["createdAt", "updatedAt"],
							},
						},
						{
							model: library.model.setting,
							as: "setting",
							attributes: {
								exclude: ["createdAt", "updatedAt"],
							},
						},
					],
				},
			],
			where: {
				id: id,
			},
			paranoid: false,
		});

		bsreResponse = {
			tte_status: "Tidak Tersedia",
			visual_tte: "Tidak Tersedia",
			tte_expired: "Tidak Tersedia",
			tte_type: "Tidak Tersedia",
			user_permission: [],
		};

		gabung = JSON.parse(JSON.stringify(users));
		gabung.profile.bsre = bsreResponse;

		if (!library.isEmpty(users)) {
			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "Profile ditemukan",
				data: gabung,
			});
		} else {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message: "Profile tidak ditemukan",
				data: {},
			});
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.count_dashboard = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		id = await library.decrypt(req.decrypted.user_login_auth);
		be = 0;
		se = 0;
		ce = 0;

		belum_tte = await library.model.task.count({
			include: [
				{
					model: library.model.document,
					as: "document",
					paranoid: true,
					required: true,
				},
			],
			where: {
				[library.Op.and]: [
					{
						id_auth: id,
					},
					{
						status_task: 0,
					},
				],
			},
		});

		sudah_tte = await library.model.task.count({
			include: [
				{
					model: library.model.document,
					as: "document",
					paranoid: true,
					required: true,
				},
			],
			where: {
				[library.Op.and]: [
					{
						id_auth: id,
					},
					{
						status_task: 1,
					},
				],
			},
		});

		dibatalkan = await library.model.task.count({
			include: [
				{
					model: library.model.document,
					as: "document",
					paranoid: true,
					required: true,
				},
			],
			where: {
				[library.Op.and]: [
					{
						id_auth: id,
					},
					{
						status_task: -1,
					},
				],
			},
		});

		newCount = {
			pending: belum_tte,
			tte: sudah_tte,
			batal: dibatalkan,
		};

		return res.status(library.responseStatus.OK).send({
			success: true,
			code: library.responseStatus.OK,
			message: "Data ditemukan",
			data: newCount,
		});
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.get_task = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		const { status } = req.params;
		id = await library.decrypt(req.decrypted.user_login_auth);

		var timeline = [];
		var aksi = "";
		var idUsers = "";

		var authId;
		var action;

		const PAGE_SIZE = 100;
		const currentPage = req.query.page || 1;

		const { count, rows } = await library.model.task.findAndCountAll({
			include: [
				{
					model: library.model.document,
					as: "document",
					paranoid: true,
					required: true,
					include: [
						{
							attributes: ["full_name", "photo_profile"],
							model: library.model.profile,
							as: "operator",
							include: [
								{
									attributes: ["opd_name"],
									model: library.model.opd,
									as: "opd",
								},
							],
						},
						{
							attributes: { exclude: ["createdAt", "updatedAt"] },
							model: library.model.timeline,
							as: "timeline",
							include: [
								{
									model: library.model.task,
									as: "task_timeline",
									include: [
										{
											attributes: ["full_name", "photo_profile"],
											model: library.model.profile,
											as: "profile",
											include: [
												{
													attributes: ["jabatan"],
													model: library.model.ppt,
													as: "ppt",
												},
											],
										},
									],
								},
							],
						},
					],
				},
			],
			where: {
				[library.Op.and]: [
					{
						id_auth: id,
					},
					{
						status_task: status,
					},
				],
			},
			limit: PAGE_SIZE,
			offset: (currentPage - 1) * PAGE_SIZE,
			order: [
				["createdAt", "DESC"],
				[
					library.model.document,
					library.model.timeline,
					"index_timeline",
					"asc",
				],
			],
		});

		const totalTask = count;
		const totalPages = Math.ceil(totalTask / PAGE_SIZE);
		const totalItem = count;

		gabung = JSON.parse(JSON.stringify(rows));
		urut = -1;
		urut2 = 0;

		await library.asyncForEach(gabung, async (element, index) => {
			await library.waitFor(1);
			urut++;
			urut2++;

			element.createdAt = rows[index].createdAt;
			element.updatedAt = rows[index].updatedAt;
			element.date_action = rows[index].actionDate;

			await library.asyncForEach(
				element.document.timeline,
				async (element, i) => {
					if (library.isEmpty(element.task_timeline)) {
						if (library.isEmpty(element.id_paraf)) {
							authId = element.id_tte;
							action = "TTE";
						} else if (library.isEmpty(element.id_tte)) {
							authId = element.id_paraf;
							action = "PARAF";
						}

						myProfile = await library.model.profile.findOne({
							attributes: ["full_name", "photo_profile"],
							include: [
								{
									attributes: ["jabatan"],
									model: library.model.ppt,
									as: "ppt",
								},
							],
							where: {
								id_auth: authId,
							},
						});

						newtask_timeline = {
							createdAt: null,
							updatedAt: null,
							actionDate: null,
							id: null,
							id_document: element.id_document,
							id_timeline: element.id,
							id_auth: authId,
							status_task: element.status_timeline,
							anchor_tte: element.anchor_tte,
							type_tte: action,
							file_tte: null,
							date_action: null,
							profile: {
								full_name: myProfile.full_name,
								photo_profile: myProfile.photo_profile,
								ppt: {
									jabatan: myProfile.ppt.jabatan,
								},
							},
						};

						element.task_timeline = newtask_timeline;
					}
				}
			);
		});

		return res.status(library.responseStatus.OK).json({
			success: true,
			code: library.responseStatus.OK,
			message: "Data ditemukan",
			currentPage,
			totalPages,
			totalItem,
			task: gabung,
		});
	} catch (error) {
		console.log(error);
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.tes = async (req, res) => {
	await library.generateQR();
	//await library.mergerQR();
};

exports.sign_action = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		idUser = await library.decrypt(req.decrypted.user_login_auth);

		var transactions;

		if (datas.twoFa && datas.otp != "") {
			console.log("CHECK KODE OTP");
		} else {
			const result = await library.model.sequelize.transaction(
				async (transaction) => {
					transactions = transaction;

					task = await library.model.task.findAndCountAll({
						where: {
							id: datas.id_task,
						},
						limit: 1,
					});

					document = await library.model.document.findAndCountAll({
						where: {
							id: datas.id_document,
						},
						limit: 1,
					});

					timeline = await library.model.timeline.findAndCountAll({
						where: {
							id: datas.id_timeline,
						},
						limit: 1,
					});

					if (task.count && document.count && timeline) {
						userProfile = await library.model.profile.findOne({
							include: [
								{
									model: library.model.ppt,
									as: "ppt",
								},
							],
							where: {
								id_auth: idUser.toString(),
							},
						});

						if (library.isEmpty(userProfile)) {
							if (datas.task_type == "PARAF") {
								return res.status(library.responseStatus.OK).send({
									success: false,
									code: library.responseStatus.OK,
									message: "Error 102, Pemaraf Tidak Ditemukan",
								});
							} else if (datas.task_type == "TTE") {
								return res.status(library.responseStatus.OK).send({
									success: false,
									code: library.responseStatus.OK,
									message: "Error 102, Penanda Tangan Tidak Ditemukan",
								});
							} else {
								return res.status(library.responseStatus.OK).send({
									success: false,
									code: library.responseStatus.OK,
									message: "Error 102, Hubungi Admin Kominfo!",
								});
							}
						} else {
							namaFileBaru = (await library.fileName(10)) + ".pdf";

							var datasTTE = "";

							if (datas.task_type == "PARAF") {
								datasTTE = {
									nik: userProfile.NIK,
									passphrase: datas.passphrase,
									linkQR: `${library.config.baseUrl}/verification/${datas.id_document}`,
									FileAsli: document.rows[0].file_document,
									tag_koordinat: task.rows[0].anchor_tte,
									namaFile: namaFileBaru,
									idDocument: datas.id_document,
									visual_tte: userProfile.ppt.visual_paraf,
									index_timeline: timeline.rows[0].index_timeline,
								};
							} else if (datas.task_type == "TTE") {
								datasTTE = {
									nik: userProfile.NIK,
									passphrase: datas.passphrase,
									linkQR: `${library.config.baseUrl}/verification/${datas.id_document}`,
									FileAsli: document.rows[0].file_document,
									tag_koordinat: task.rows[0].anchor_tte,
									namaFile: namaFileBaru,
									idDocument: datas.id_document,
									visual_tte: userProfile.ppt.visual_tte,
									index_timeline: timeline.rows[0].index_timeline,
								};
							}

							statusTTE = await library.getStatusTTE(userProfile.NIK);

							if (statusTTE[0].status_code == 1111) {
								a = ".";
								b = datasTTE.FileAsli.replace("assets", "public");
								c = datasTTE.visual_tte.replace("assets", "public");
								var filesss = a.concat(b);
								var imageParaf = a.concat(c);

								if (!library.fs.existsSync(imageParaf)) {
									return res.status(library.responseStatus.OK).send({
										success: false,
										code: library.responseStatus.OK,
										message:
											"Error 104, Visual Paraf Anda Tidak Ditemukan, Silahkan Hubungi Admin Kominfo",
									});
								}

								if (datas.task_type == "PARAF") {
									coordinate = await library.addVisual(
										filesss,
										datasTTE.tag_koordinat
									);

									addPrf = await library.addParaf(
										filesss,
										datasTTE.namaFile,
										coordinate.data,
										imageParaf
									);

									datasTTE.FileAsli = addPrf;
									datasTTE.visual_tte = imageParaf;

									paraf = await library.paraf(datasTTE);
								} else {
									coordinate = await library.addVisual(
										filesss,
										datasTTE.tag_koordinat
									);

									console.log("DISINI");

									await library.generateQR(datasTTE);

									/* addTTE = await library.addTTE(datasTTE);

									datasTTE.FileAsli = addTTE;
									datasTTE.visual_tte = imageParaf;

									tte = await library.tte(datasTTE); */
								}

								nextTimeline = await library.model.timeline.findOne({
									where: {
										[library.Op.and]: [
											{
												id_document: datas.id_document.toString(),
											},
											{
												index_timeline:
													parseInt(document.rows[0].status_index) + 1,
											},
										],
									},
								});

								if (library.isEmpty(nextTimeline)) {
									if (datas.task_type == "PARAF") {
										return res.status(library.responseStatus.OK).send({
											success: false,
											code: library.responseStatus.OK,
											message:
												"Error 103, Pemaraf/Penandatangan Selanjutnya Tidak Ada, Silahkan Hubungi Admin Kominfo!",
										});
									} else if (datas.task_type == "TTE") {
										console.log("NEXT TTE");
									}
								} else {
									let id_user;
									if (library.isEmpty(nextTimeline.id_paraf)) {
										id_user = nextTimeline.id_tte;
										type_task = "TTE";
									} else if (library.isEmpty(nextTimeline.id_tte)) {
										id_user = nextTimeline.id_paraf;
										type_task = "PARAF";
									} else if (
										document.rows[0].jenis_document == "telaahaan_staf"
									) {
										type_task = "TELAAHAAN";
									}

									newTask = {
										id: await library.uuid(),
										id_document: datas.id_document,
										id_timeline: datas.id_timeline,
										id_auth: id_user,
										status_task: 0,
										anchor_tte: nextTimeline.anchor_tte,
										type_tte: type_task,
										file_tte: null,
										date_action: null,
									};

									await library.model.task.create(newTask, {
										transaction: transactions,
									});

									updateTask = {
										status_task: 1,
										file_tte: paraf,
										date_action: library.moment().format("YYYY-MM-DD HH:mm:ss"),
									};

									await library.model.task.update(updateTask, {
										where: {
											id: datas.id_task,
										},
										transaction: transactions,
									});

									uTimeline = {
										status_timeline: 1,
										date_action: library.moment().format("YYYY-MM-DD HH:mm:ss"),
									};

									await library.model.timeline.update(uTimeline, {
										where: {
											id: datas.id_timeline,
										},
										transaction: transactions,
									});

									uDocument = {
										status_document: "Telah Diparaf",
										file_document: paraf,
										status_index: parseInt(document.rows[0].status_index) + 1,
										file_tte: document.rows[0].file_document,
									};

									upDate = await library.model.document.update(uDocument, {
										transaction: transactions,
										where: {
											id: datas.id_document,
										},
									});

									if (
										library.fs.existsSync(
											"./public/upload/temp/TEMP_PARAF_" + namaFileBaru
										)
									) {
										library.fs.unlinkSync(
											"./public/upload/temp/TEMP_PARAF_" + namaFileBaru
										);
									}

									if (!library.isEmpty(upDate)) {
										return res.status(library.responseStatus.OK).send({
											success: true,
											code: library.responseStatus.OK,
											message: "Yeay, Dokumen Berhasil Diparaf",
										});
									} else {
										return res.status(library.responseStatus.OK).send({
											success: false,
											code: library.responseStatus.OK,
											message: "Dokumen Gagal Diparaf, Silahkan Coba Lagi!",
										});
									}
								}
							} else if (
								statusTTE[0].status_code == 1110 ||
								statusTTE[0].status_code == 2021
							) {
								return res.status(library.responseStatus.OK).send({
									success: false,
									code: library.responseStatus.OK,
									message:
										"Sertifikat Elektronik Anda Belum Aktif, Sehingga Tidak Dapat Melakukan Paraf/Tandatangan Dokumen, Silahkan Hubungi Admin Kominfo Untuk Aktivasi Akun TTE",
								});
							} else {
								return res.status(library.responseStatus.OK).send({
									success: false,
									code: library.responseStatus.OK,
									message:
										"Sertifikat Elektronik Anda Belum Aktif, Sehingga Tidak Dapat Melakukan Paraf/Tandatangan Dokumen Silahkan Hubungi Admin Kominfo Untuk Aktivasi Akun TTE",
								});
							}
						}
					} else {
						return res.status(library.responseStatus.OK).send({
							success: false,
							code: library.responseStatus.OK,
							message: "Error 101, Format Salah!",
						});
					}
				}
			);
		}
	} catch (error) {
		if (!library.isEmpty(error) && !library.isEmpty(error.response)) {
			var decodedString = String.fromCharCode.apply(
				null,
				new Uint8Array(error.response.data)
			);
			var obj = JSON.parse(decodedString);

			if (obj.error == "Proses signing gagal : Passphrase anda salah 2031") {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Passphrase Anda Salah, Silahkan Check Kembali!",
				});
			} else {
				return res.status(library.responseStatus.serverError).send({
					success: false,
					code: library.responseStatus.serverError,
					message: `ERROR ${obj.status_code}, ${obj.error}`,
				});
			}
		} else {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	}
};

exports.sendOtp = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		idUser = await library.decrypt(req.decrypted.user_login_auth);

		let setting = await library.model.setting.findOne({
			where: {
				id_auth: idUser,
			},
		});

		if (setting.twofa) {
			if (await library.sendOtp("6281269494591")) {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "OTP Berhasil Dikirim Ke Nomor Whatsapp Anda",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "OTP Gagal Dikirim Ke Nomor Whatsapp Anda",
				});
			}
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.create_session = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		idUser = await library.decrypt(req.decrypted.user_login_auth);

		const monthList = [
			"01",
			"02",
			"03",
			"04",
			"05",
			"06",
			"07",
			"08",
			"09",
			"10",
			"11",
			"12",
		];

		const d = new Date();
		let month = monthList[d.getMonth()];

		cMonth = library.moment().month() + 1;
		cYear = library.moment().year();

		if (cMonth >= 1 && cMonth <= 9) {
			cMonth = `0${cMonth}`;
		}

		countReport = await library.model.session.count({
			where: {
				[library.Op.and]: [
					{
						month_period: cMonth,
					},
					{
						year_perioed: cYear,
					},
					{
						id_auth: idUser,
					},
				],
			},
			limit: 2,
		});

		if (countReport == 2) {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message:
					"Anda Telah Melebihi Batas Pelaporan Untuk Hari ini, Jika Ada laporan yang salah, silahkan hubungi admin",
			});
		} else {
			addSession = {
				id: await library.uuid(),
				id_auth: idUser,
				id_session: await library.uuid(),
				month_period: month,
				year_perioed: new Date().getFullYear(),
				status: "Record",
			};

			insert = await library.model.session.create(addSession);

			if (!library.isEmpty(insert)) {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "Sesi Berhasil Dibuat",
					id_session: insert.id,
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Gagal Membuat Sesi",
				});
			}
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.update_track = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		idUser = await library.decrypt(req.decrypted.user_login_auth);

		newTrack = {
			id: await library.uuid(),
			id_session: datas.idSession,
			id_auth: idUser,
			lat_track: datas.latitude,
			lng_track: datas.longitude,
		};

		insert = await library.model.tracking.create(newTrack);

		if (!library.isEmpty(insert)) {
			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "Tracking Update!",
			});
		} else {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message: "Tracking Update Gagal",
			});
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.report = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		params = JSON.parse(JSON.stringify(req.params));
		query = JSON.parse(JSON.stringify(req.query));
		files = JSON.parse(JSON.stringify(req.files));

		idUser = await library.decrypt(req.decrypted.user_login_auth);

		if (
			library.isEmpty(datas.keterangan) ||
			library.isEmpty(datas.type_report) ||
			library.isEmpty(datas.latitude) ||
			library.isEmpty(datas.longitude) ||
			library.isEmpty(datas.idSession)
		) {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message: "Error, Format Salah",
			});
		} else {
			await library
				.sharp(files[0].path)
				.jpeg({
					quality: 60,
				})
				.toFile(
					library.path.resolve(
						"./public/upload/",
						"tracking_report",
						files[0].filename
					)
				);

			if (files.length > 0) {
				library.fs.unlinkSync(files[0].path);
			}

			let now = library.moment();

			addLocation = {
				id: await library.uuid(),
				id_session: datas.idSession,
				id_auth: idUser,
				lat_track: datas.latitude,
				lng_track: datas.longitude,
				is_report: true,
				type_report: datas.type_report,
				image_report: `/assets/upload/tracking_report/${files[0].filename}`,
				desc_report: datas.keterangan,
				date_time_report: now.format("YYYY-MM-DD HH:mm:ss"),
			};

			insert = await library.model.tracking.create(addLocation);

			if (!library.isEmpty(insert)) {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: "Sukses, Laporan Berhasil Di Upload",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Error, Laporan Gagal Di Upload",
				});
			}
		}
	} catch (error) {
		console.log(error);
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.record = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		params = JSON.parse(JSON.stringify(req.params));
		query = JSON.parse(JSON.stringify(req.query));

		idUser = await library.decrypt(req.decrypted.user_login_auth);

		updateStatus = {
			status: params.action,
		};

		await library.model.session.update(updateStatus, {
			where: { id: datas.id_session },
		});

		return res.status(library.responseStatus.OK).send({
			success: true,
			code: library.responseStatus.OK,
			message: "OKE",
		});
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.get_session_status = async (req, res) => {
	try {
		await library.formData(req, res);
		datas = JSON.parse(JSON.stringify(req.body));
		params = JSON.parse(JSON.stringify(req.params));
		query = JSON.parse(JSON.stringify(req.query));

		session = await library.model.session.findOne({
			include: [
				{
					model: library.model.tracking,
					as: "tracking",
					where: {
						[library.Op.or]: [
							{
								type_report: 2,
							},
							{
								type_report: null,
							},
						],
					},
				},
			],
			where: {
				[library.Op.and]: [
					{
						id: params.session,
					},
					{
						status: "Pause",
					},
				],
			},
			order: [
				["createdAt", "ASC"],
				[library.model.tracking, "createdAt", "asc"],
			],
		});

		if (library.isEmpty(session)) {
			return res.status(library.responseStatus.OK).send({
				success: false,
				code: library.responseStatus.OK,
				message: "OK",
				data: session,
			});
		} else {
			return res.status(library.responseStatus.OK).send({
				success: true,
				code: library.responseStatus.OK,
				message: "OK",
				data: session,
			});
		}
	} catch (error) {
		console.log(error);
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};
