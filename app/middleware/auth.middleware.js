const library = require("../core/library");

exports.auth = async (req, res, next) => {
	let session = req.session;
	if (!library.isEmpty(session.tokenLogin)) {
		res.status(library.responseStatus.OK);
		return res.redirect("/dashboard");
	}
	next();
};

exports.api_key = async (req, res, next) => {
	try {
		datas = JSON.parse(JSON.stringify(req.body));

		token = library.isEmpty(req.headers.authorization);
		packageApp = library.isEmpty(req.headers["package-app"]);

		if (token || packageApp) {
			return res.status(library.responseStatus.unauthorized).send({
				success: false,
				code: library.responseStatus.unauthorized,
				message: "Format Salah!!!, hubungi admin kominfo",
			});
		} else {
			apiKey = req.headers.authorization.split(" ");

			if (apiKey[0] != "Bearer") {
				return res.status(library.responseStatus.unauthorized).send({
					success: false,
					code: library.responseStatus.unauthorized,
					message: "Format Akses Anda Salah!",
				});
			} else {
				check_api_key = await library.model.app_setting.findOne({
					where: {
						[library.Op.and]: [
							{
								app_token: apiKey[1],
							},
							{
								app_package: req.headers["package-app"],
							},
						],
					},
				});

				if (library.isEmpty(check_api_key)) {
					return res.status(library.responseStatus.unauthorized).send({
						success: false,
						code: library.responseStatus.unauthorized,
						message:
							"Anda Tidak Memiliki Akses Untuk API Ini, Silahkan Hubungi Admin Kominfo",
					});
				} else {
					next();
				}
			}
		}
	} catch (error) {
		return res.status(library.responseStatus.serverError).send({
			success: false,
			code: library.responseStatus.serverError,
			message: error.message,
		});
	}
};

exports.unauth = async (req, res, next) => {
	let session = req.session;

	if (library.isEmpty(session.tokenLogin)) {
		res.status(library.responseStatus.OK);
		return res.redirect("/login");
	} else {
		const secret = Buffer.from(library.config.hex, "hex");
		await library
			.decryptJwt(session.tokenLogin.split(" ")[1], secret)
			.then((data) => {
				req.decrypted = data;
				next();
			})
			.catch((err) => {
				res.status(library.responseStatus.OK);
				return res.redirect("/login");
			});
	}
};

exports.mode = async (req, res, next) => {
	if (library.config.mode == "Maintenance") {
		res.status(library.responseStatus.OK);
		return res.redirect("/maintenance");
	}
	next();
};

exports.auth_check = async (req, res, next) => {
	try {
		const secret = Buffer.from(library.config.hex, "hex");
		token = req.headers["user-agent"];
		const { payload, protectedHeader } = await library.decryptJwt(
			token.split(" ")[1],
			secret
		);

		req.decrypted = payload;

		next();
	} catch (error) {
		return res.status(library.responseStatus.unauthorized).send({
			success: false,
			code: library.responseStatus.unauthorized,
			message: error.code,
		});
	}
};

exports.mode_android = async (req, res, next) => {
	if (library.config.mode == "Maintenance") {
		return res.status(library.responseStatus.OK).send({
			success: false,
			code: library.responseStatus.OK,
			message: "Aplikasi Sedang Maintenance",
		});
	}
	next();
};

exports.checkMaintenance = async (req, res, next) => {
	if (library.config.mode != "Maintenance") {
		res.status(200);
		return res.redirect("/");
	}
	next();
};
