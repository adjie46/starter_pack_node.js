const express = require("express");
const { web_setting } = require("../middleware/web_setting");
const route = express.Router();

const webSetting = require("../middleware/web_setting");
const authMiddleware = require("../middleware/auth.middleware");

const errorController = require("../controller/error.controller");
const authController = require("../controller/auth.controller");
const dashboardController = require("../controller/dashboard.controller");
const userController = require("../controller/user.controller");
const settingController = require("../controller/setting.controller");

route.get(
	"/404",
	authMiddleware.mode,
	webSetting.web_setting,
	errorController.err_404
);

route.get(
	"/maintenance",
	authMiddleware.checkMaintenance,
	webSetting.web_setting,
	errorController.maintenance
);

route.get(
	"/login",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.auth,
	authController.login_pages
);
route.get(
	"/",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.auth,
	authController.login_pages
);

route.get(
	"/dashboard",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	dashboardController.dashboard_pages
);

//AUTH
route.post(
	"/login",
	authMiddleware.mode,
	webSetting.web_setting,
	authController.login_action
);

route.get(
	"/logout",
	authMiddleware.mode,
	webSetting.web_setting,
	authController.authLogout
);

//USER
route.get(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

route.post(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

route.put(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

route.delete(
	"/user/:id/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.profileAction
);

//SETTING
route.get(
	"/setting/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.setting
);

route.post(
	"/setting/:action",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.add
);

route.delete(
	"/setting/:action/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.delete
);

route.put(
	"/setting/:action/:id",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	settingController.copy
);

route.post(
	"/profile/change_photo",
	authMiddleware.mode,
	authMiddleware.unauth,
	webSetting.web_setting,
	userController.change_photo
);

route.all(
	"*",
	authMiddleware.mode,
	webSetting.web_setting,
	errorController.err_404
);

module.exports = route;
