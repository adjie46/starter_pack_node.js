const express = require("express");
const route = express.Router();

const authMiddleware = require("../middleware/auth.middleware");

const appController = require("../controller/app.controller");
const errorController = require("../controller/error.controller");

route.post(
	"/update",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	appController.update_app
);

route.post(
	"/auth",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	appController.login_android
);

route.get(
	"/profile",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.getProfileById
);

route.get(
	"/dashboard",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.count_dashboard
);

route.get(
	"/inbox/:status",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.get_task
);

route.post(
	"/sign",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.sign_action
);

route.post(
	"/otp",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.sendOtp
);

route.post(
	"/session",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.create_session
);

route.post(
	"/update_track",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.update_track
);

route.post(
	"/report",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.report
);

route.put(
	"/record/:action",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.record
);

route.get(
	"/session/status/:session",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.get_session_status
);

route.get(
	"/test",
	authMiddleware.mode_android,
	authMiddleware.api_key,
	authMiddleware.auth_check,
	appController.tes
);

route.all("*", authMiddleware.mode_android, errorController.err_404_android);

module.exports = route;
