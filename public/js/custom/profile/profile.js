$(document).ready(function () {
	var m = $("meta[name=X-ID-Compatible]");
	var n = $("meta[name=X-ID-Indonesia]");

	function getUrlVars() {
		var vars = [],
			hash;
		var hashes = window.location.href
			.slice(window.location.href.indexOf("?") + 1)
			.split("&");
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split("=");
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	var id = getUrlVars()["id"];

	$.ajax({
		type: "get",
		headers: {
			"X-CSRF-Token": m.attr("content"),
		},
		url: `/user/${id}/profile`,
		dataType: "json",
		beforeSend: function () {
			$("#section_loading_photo").show();
			$("#section_loading_device").show();
			$("#section_loading_profile").show();
			$("#section_photo").hide();
			$(".section_device").hide();
			$(".section_profile").hide();
		},
		success: function (response) {
			setTimeout(() => {
				if (response.success) {
					$("#section_loading_photo").hide();
					$("#section_loading_device").hide();
					$("#section_loading_profile").hide();
					$("#section_photo").show();
					$(".section_device").show();
					$(".section_profile").show();

					$("#profile_picture").attr(
						"src",
						response.data.profile.photo_profile
					);

					$("#profile_picture").on("error", function () {
						$("#profile_picture").attr(
							"src",
							"../../dist/images/profile/user-1.jpg"
						);
					});

					$(".full_name").html(response.data.profile.full_name);

					if (
						response.data.profile.ppt == "null" ||
						response.data.profile.ppt == null
					) {
						$("#change_photo_profile").css("visibility", "hidden");
						$("#change_visual_tte").css("visibility", "hidden");
					} else {
						$(".pangkat_golongan").html(`${response.data.profile.phone}`);

						$(".nik_user").html(response.data.profile.ppt.nik);
						$(".nip_user").html(response.data.profile.ppt.nip);
						$(".jabatan_user").html(response.data.profile.ppt.jabatan);
						$(".golongan_user").html(
							`${response.data.profile.email},${response.data.profile.ppt.golongan}`
						);
					}

					if (response.data.profile.device.length > 0) {
						response.data.profile.device.forEach((element) => {
							$("#device_list").append(`
							<div class="d-flex align-items-center justify-content-between py-3 border-bottom">
                                <div class="d-flex align-items-center gap-3">
                                    <i class="ti ti-device-mobile text-dark d-block fs-7" width="26" height="26"></i>
                                    <div>
                                        <h5 class="fs-4 fw-semibold mb-0">${element.manufacturer} ${element.type}</h5>
                                        <p class="mb-0">OS Name : ${element.os_name}, Hardware : ${element.hardware}</p>
                                    </div>
                                </div>
                                <a id="delete_device" data-id='${element.id}' class="text-dark fs-6 d-flex align-items-center justify-content-center bg-transparent p-2 fs-4 rounded-circle"
                                    href="javascript:void(0)">
                                    <i class="ti ti-square-rounded-x-filled"></i>
                                </a>
                            </div>
						`);
						});
					} else {
						$("#device_list").append(`
							<div class="d-flex align-items-center justify-content-between py-3 border-bottom">
								<span>Device Tidak ditemukan</span>
							</div>
						`);
					}

					$(".opd_user").html(response.data.profile.opd.opd_name);
					$(".email_user").html(response.data.profile.email);
					$(".phone_user").html(response.data.profile.phone);
				} else {
				}
			}, 1000);
		},
		error: function (xhr, status, errorThrown) {
			if (xhr.statusText == "Unauthorized") {
				location.replace("/");
			} else {
				Swal.fire({
					type: "error",
					title: "<b>Gagal!</b>",
					html: xhr.responseJSON.message,
					confirmButtonText: "OK",
				}).then((result) => {
					location.reload();
				});
			}
		},
	});

	/* $.ajax({
		type: "get",
		headers: {
			"X-CSRF-Token": m.attr("content"),
		},
		url: `/user/${id}/bsre_profile`,
		dataType: "json",
		beforeSend: function () {
			$("#section_loading_bsre").show();
			$(".section_bsre").hide();
		},
		success: function (response) {
			setTimeout(() => {
				if (response.success) {
					console.log(response);

					$("#section_loading_bsre").hide();
					$(".section_bsre").show();

					$("#status_tte").html(response.data.tte_status);
					$("#expired_tte").html(response.data.tte_expired);
					$("#tte_type").html(response.data.tte_type);

					$("#visual_tte_button").attr("href", response.data.visual_tte);

					response.data.user_permission.forEach((element) => {
						$("#permission_section").append(`
							<span class="badge bg-info">${element.permission_name}</span>
						`);
					});
				} else {
				}
			}, 1000);
		},
		error: function (xhr, status, errorThrown) {
			if (xhr.statusText == "Unauthorized") {
				location.replace("/");
			} else {
				Swal.fire({
					type: "error",
					title: "<b>Gagal!</b>",
					html: xhr.responseJSON.message,
					confirmButtonText: "OK",
				}).then((result) => {
					location.reload();
				});
			}
		},
	}); */

	$(document).on("click", "#delete_device", function () {
		var id = $(this).attr("data-id");

		Swal.fire({
			title: "Yakin ingin menghapus device ini?",
			showCancelButton: !0,
			type: "warning",
			confirmButtonText: "Ya, hapus!",
			showLoaderOnConfirm: !0,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: async () => {
				return fetch(`/user/${id}/delete_device`, {
					method: "delete",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.status) {
							throw new Error(response.message);
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error}`);
					});
			},
			allowOutsideClick: !1,
		}).then((result) => {
			if (result.value.success) {
				Swal.fire("Berhasil!", result.value.message, "success").then(
					(result) => {
						location.reload();
					}
				);
			} else {
				Swal.fire("Gagal!", result.value.message, "error").then((result) => {
					location.reload();
				});
			}
		});
	});

	$(document).on("click", "#change_photo_profile", function () {
		$("#photos").trigger("click");

		var currentUrl = window.location.href;

		// Parse the URL to extract the ID
		var urlObject = new URL(currentUrl);
		var id = urlObject.searchParams.get("id");

		$("#photos").on("change", function () {
			var formData = new FormData();
			formData.append("files", $("input[type=file]")[0].files[0]);
			formData.append("id_auth", id);

			$.ajax({
				type: "post",
				headers: {
					"X-CSRF-Token": m.attr("content"),
				},
				url: "/profile/change_photo",
				data: formData,
				contentType: false,
				processData: false,
				dataType: "json",
				beforeSend: function () {
					$("body").LoadingOverlay("show", {
						background: "rgba(165, 190, 100, 0.5)",
					});
				},
				success: function (response) {
					if (response.success) {
						$("body").LoadingOverlay("hide", true);
						$("#addOpd").modal("toggle");
						Swal.fire({
							type: "success",
							title: "<b>Sukses!</b>",
							html: response.message,
							confirmButtonText: "OK",
						}).then((result) => {
							location.reload();
						});
					} else {
						$("#addOpd").modal("toggle");
						$("body").LoadingOverlay("hide", true);
						Swal.fire({
							type: "error",
							title: "<b>Gagal!</b>",
							html: response.message,
							confirmButtonText: "OK",
						}).then((result) => {
							location.reload();
						});
					}
				},
				error: function (xhr, status, errorThrown) {
					if (xhr.statusText == "Unauthorized") {
						location.replace("/");
					} else {
						$("body").LoadingOverlay("hide", true);
						$("#addOpd").modal("toggle");
						Swal.fire({
							type: "error",
							title: "<b>Gagal!</b>",
							html: xhr.responseJSON.message,
							confirmButtonText: "OK",
						}).then((result) => {
							location.reload();
						});
					}
				},
			});
		});
	});
});
